package com.innovpk.app.pkframes.ui;


import android.Manifest;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.widget.ImageView;


import com.innovpk.app.pkframes.BitmapHelper;
import com.innovpk.app.pkframes.R;
import com.innovpk.app.pkframes.ui.widget.CameraPreview;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static com.innovpk.app.pkframes.CameraHelper.cameraAvailable;
import static com.innovpk.app.pkframes.CameraHelper.getCameraInstance;
import static com.innovpk.app.pkframes.MediaHelper.getOutputMediaFile;
import static com.innovpk.app.pkframes.MediaHelper.saveToFile;

public class CameraActivity extends Activity implements Camera.PictureCallback {

    private static final String TAG = "CameraActivity";
    public static float aspectRatio; // LH: get the aspectRatio of the cameraImage
    public static int targetHeight = 1980;
    public static int targetWidth;
    private Camera camera;

    ViewPager viewPager;
    CameraFrameAdapter adapter;
    CameraPreview cameraPreview;
    int[] mResources = {
            R.drawable.frame1,
            R.drawable.frame2,
            R.drawable.frame3,
            R.drawable.frame4,
            R.drawable.frame5
    };
    private static int framePos;
    private boolean cameraFront = false;
    BitmapHelper cameraBitMap = new BitmapHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ContextCompat.checkSelfPermission(CameraActivity.this, Manifest.permission.CAMERA);
        setResult(RESULT_CANCELED);
        // Camera may be in use by another activity or the system or not available at all
        camera = getCameraInstance();
        if (cameraAvailable(camera)) {
            initCameraPreview();
            setCameraDisplayOrientation(this, findBackFacingCamera(), camera);
            getCameraAspectRatio();
        } else {
            finish();
        }

        final ImageView swapButtoView = (ImageView) findViewById(R.id.swapCamera);

        swapButtoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                releaseCamera();
                chooseCamera();

            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewPagerFrame);
        adapter = new CameraFrameAdapter(this);
        viewPager.setAdapter(adapter);
        viewPager.getCurrentItem();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                framePos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void getCameraAspectRatio() {
        Camera.Parameters parameters = camera.getParameters();
        int picH = parameters.getPictureSize().height;
        int picW = parameters.getPictureSize().width;
        aspectRatio = ((float)(picH)) / ((float)(picW));
        targetWidth = Math.round(targetHeight * aspectRatio);
        Log.d(TAG, "aspectRatio" + aspectRatio + "targetHeight" + targetHeight + "targetWidth " + targetWidth);
    }

    public static int getFramePos() {

        return framePos;
    }

    public static void setFrame(int frame) {
        framePos = frame;

    }

    private int findFrontFacingCamera() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = 0;
        numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                cameraFront = true;
                break;
            }
        }
        return cameraId;
    }

    private int findBackFacingCamera() {
        int cameraId = -1;
        //Search for the back facing camera
        //get the number of cameras
        int numberOfCameras = Camera.getNumberOfCameras();
        //for every camera check
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                cameraFront = false;
                break;
            }
        }
        return cameraId;
    }

    public void chooseCamera() {
        //if the camera preview is the front
        if (cameraFront) {
            int cameraId = findBackFacingCamera();
            if (cameraId >= 0) {
                camera = Camera.open(cameraId);

                try {

                    camera.setPreviewDisplay(cameraPreview.getHolder());

                } catch (IOException e) {
                    e.printStackTrace();
                }
                setCameraDisplayOrientation(this, cameraId, camera);
                camera.startPreview();
            }
        } else {
            int cameraId = findFrontFacingCamera();
            if (cameraId >= 0) {
                //open the backFacingCamera
                //set a picture callback
                //refresh the preview
                camera = Camera.open(cameraId);
                try {

                    camera.setPreviewDisplay(cameraPreview.getHolder());

                } catch (IOException e) {
                    e.printStackTrace();
                }
                setCameraDisplayOrientation(this, cameraId, camera);
                camera.startPreview();

            }
        }
    }

    // Show the camera view on the activity
    private void initCameraPreview() {
        cameraPreview = (CameraPreview) findViewById(R.id.camera_preview);
        cameraPreview.init(camera);

    }


    public void onCaptureClick(View button) {
        // Take a picture with a callback when the photo has been created
        // Here you can add callbacks if you want to give feedback when the picture is being taken

        camera.takePicture(null, null, this);

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

        String path = savePictureToFileSystem(data);

        setResult(path, data);
        finish();
    }

    public static String savePictureToFileSystem(byte[] data) {
        File file = getOutputMediaFile();
        saveToFile(data, file);
        Log.d("saving image", file.getAbsolutePath().toString());
        return file.getAbsolutePath();
    }

    private void setResult(String path, byte[] data) {

        displayImage(path);
    }

    // ALWAYS remember to release the camera when you are finished
    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    private void releaseCamera() {
        if (camera != null) {
            camera.release();
            camera = null;

        }
    }


    public static void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing

            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    private void displayImage(String path) {


        Resources r = getResources();
        Drawable[] layers = new Drawable[2];
        Drawable frameLayer = r.getDrawable(mResources[getFramePos()]);
        Bitmap frameBitmap = ((BitmapDrawable)frameLayer).getBitmap();
        Drawable frameLayerSc = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(frameBitmap, targetWidth, targetHeight, true));
        Log.d(TAG, "frameLayer height" + frameLayer.getIntrinsicHeight());
        Log.d(TAG, "frameLayerSc height" + frameLayerSc.getIntrinsicHeight());

        CameraActivity.setFrame(0);
        int rotate = 0;

        Bitmap bitmap = BitmapHelper.decodeSampledBitmap(path, targetWidth, targetHeight);
        Log.d(TAG, "bitmap width" + bitmap.getWidth() + "bitmap heigth " + bitmap.getHeight()); // Why is it from front camera 640*640?


        try {

            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case 1:
                    rotate = 90;
                    break;
                case 0:
                    rotate = 90;
            }

            Log.e(" Exit ", "Exif orientation: " + orientation);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Matrix mtx = new Matrix();
        if (cameraFront == true) {

            mtx.preScale(-1.0f, 1.0f);
            mtx.postRotate(90);

        } else {

            mtx.postRotate(rotate);

        }

        Bitmap rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, true);

        Drawable cameraImg = new BitmapDrawable(getResources(), rotated);
        Log.d(TAG, "cameraImg Height" + cameraImg.getIntrinsicHeight() + "width " + cameraImg.getIntrinsicWidth());


        BitmapDrawable bd=(BitmapDrawable) this.getResources().getDrawable(mResources[getFramePos()]);
        /*double imageHeight = bd.getBitmap().getHeight();
        double imageWidth = bd.getBitmap().getWidth();*/
        double imageHeight = targetHeight;
        double imageWidth = targetWidth;
        Log.d(TAG,"Frame drawable size"+imageWidth +"  "+ imageHeight);

        layers[0] = cameraImg;
        layers[1] = frameLayerSc;


        Log.d(TAG,"frameLayer"+ frameLayerSc.getIntrinsicHeight() +"  "+ frameLayerSc.getIntrinsicWidth());


        saveFilteredImage(layers[0], layers[1]);
        boolean deleted = false;
        try {
            File file = new File(path);
            deleted = file.delete();

        } catch (Exception ex) {
            Log.d("exception in deleting file at location:", path);
        }

        if (deleted) {
            Log.d("successful in deleting file at location:", path);
        }


    }

    public static Bitmap getOverlayedImage(Resources res, Drawable img1, Drawable img2) {
        float den = res.getDisplayMetrics().density;
        int dip = (int) (20 * den + 0.5f); // not necessary here anymore. Could be included
        // when the targetWidth and height are created so that they are not hardcoded but density-dependent

        //int sz = (int) (16 * dip + 0.5f); // LH: This caused the image to be a square because it
        // was given as width AND height parameter to the bitmap - we need two different values here
        // I assign instead targetWidth and targetHeight to the bitmap

        Drawable[] layers = new Drawable[2];
        layers[0] = img1;
        layers[1] = img2;

        LayerDrawable layerDrawable = new LayerDrawable(layers);
        layerDrawable.setLayerInset(1, 0, 0, 0, 0);

        Bitmap b = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);
        Log.d(TAG,"Width and height of overlayed image"+b.getWidth()+" "+b.getHeight());
        layerDrawable.setBounds(0, 0, targetWidth, targetHeight);
        layerDrawable.draw(new Canvas(b));

        return b;
    }

    private void saveFilteredImage(Drawable img1, Drawable img2) {

        Bitmap bitmap = getOverlayedImage(getResources(), img1, img2);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] bitmapdata = stream.toByteArray();

        String path = CameraActivity.savePictureToFileSystem(bitmapdata);

        Drawable cameraImg = new BitmapDrawable(getResources(), BitmapHelper.decodeSampledBitmap(path, targetWidth, targetHeight));


    }
}
