package com.innovpk.app.pkframes.ui.widget;

/**
 * Created by Admin on 14.07.2017.
 */

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.hardware.Camera.AutoFocusCallback;



public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private Camera camera;
    private SurfaceHolder holder;
    private AutoFocusCallback autoFocusCallback;

    public CameraPreview(Context context, AttributeSet attrs, int defStyle , AutoFocusCallback autoFocusCb) {
        super(context, attrs, defStyle);
          autoFocusCallback = autoFocusCb;
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraPreview(Context context) {
        super(context);
    }

    public void init(Camera camera) {
        this.camera = camera;
        initSurfaceHolder();
    }

    @SuppressWarnings("deprecation") // needed for < 3.0
    private void initSurfaceHolder() {
        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        initCamera(holder);
    }

    private void initCamera(SurfaceHolder holder) {
        try {
            //  camera.setDisplayOrientation(90);
            camera.setPreviewDisplay(holder);
            camera.startPreview();
            camera.autoFocus(autoFocusCallback);
        } catch (Exception e) {
            Log.d("Error setting camera preview", String.valueOf(e));
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // not-used
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // not-used
    }
}
