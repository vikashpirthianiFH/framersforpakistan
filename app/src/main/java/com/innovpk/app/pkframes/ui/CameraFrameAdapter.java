package com.innovpk.app.pkframes.ui;

/**
 * Created by Admin on 14.07.2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.innovpk.app.pkframes.R;


public class CameraFrameAdapter extends PagerAdapter {

    private int [] image_resources = {R.drawable.frame1,R.drawable.frame2,R.drawable.frame3, R.drawable.frame4, R.drawable.frame5
    };
    private Context ctx;
    private LayoutInflater layoutInflater;
    public CameraFrameAdapter(Context ctx){
        this.ctx  = ctx;

    }
    @Override
    public int getCount() {
        return image_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view ==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view =layoutInflater.inflate(R.layout.swipe_frame_layout, container, false);
        ImageView imageView = (ImageView)item_view.findViewById(R.id.imageView);
        imageView.setImageResource(image_resources[position]);
        container.addView(item_view);
        return item_view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}