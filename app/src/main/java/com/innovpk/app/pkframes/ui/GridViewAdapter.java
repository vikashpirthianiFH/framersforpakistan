package com.innovpk.app.pkframes.ui;

/**
 * Created by Admin on 14.07.2017.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;


import com.innovpk.app.pkframes.R;

import java.util.ArrayList;
import java.util.List;



public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private List<Drawable> data = new ArrayList<Drawable>();
    private int resourceId;
    LayoutInflater inflater ;


    public GridViewAdapter(Context context, int resourceId, List<Drawable> data) {
        super(context, resourceId, data);
        this.resourceId = resourceId;
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);

    }


    static class ViewHolder {
        ImageView image;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        if (row == null) {
            row = inflater.inflate(resourceId, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) row.findViewById(R.id.image);
            holder.image.setImageDrawable(data.get(position));
            row.setTag(holder);

        } else {
            holder = (ViewHolder) row.getTag();
        }

        return row;
    }


}