package com.innovpk.app.pkframes.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


import com.innovpk.app.pkframes.BitmapHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.innovpk.app.pkframes.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ImageGalleryActivity extends AppCompatActivity {

    private AdView mGalleryView;

    private GridView grid;
    private List<Drawable> imageItems= new ArrayList<>();;
    private GridViewAdapter gridAdapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);

        final File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getString(R.string.folder_name));
        traverse(mediaStorageDir);
        grid = ( GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this,  R.layout.grid_item, imageItems);
        grid.setAdapter(gridAdapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent,
                                    View v, int position, long id){

                // Send intent to SingleViewActivity

                Drawable currentDrawable = imageItems.get(position);
                Bitmap bitmap= ((BitmapDrawable)currentDrawable).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                byte[] b = baos.toByteArray();
                Intent intent=new Intent(ImageGalleryActivity.this,SingleViewActivity.class);
                intent.putExtra("picture", b);
                intent.putExtra("position", position);
                startActivity(intent);
                finish();

            }
        });
        mGalleryView = (AdView) findViewById(R.id.galleryAd);

        AdRequest adRequest = new AdRequest.Builder()

                .build();

        mGalleryView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mGalleryView.loadAd(adRequest);

    }
    public void traverse (File dir) {
        if (dir.exists()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; ++i) {
                File file = files[i];
                if (file.isDirectory()) {
                    traverse(file);
                } else {
                    Drawable cameraImg = new BitmapDrawable(getResources(), BitmapHelper.decodeSampledBitmap(file.getAbsolutePath(), CameraActivity.targetWidth, CameraActivity.targetHeight));

                    imageItems.add(cameraImg);
                }
            }
        }
    }

    @Override
    public void onPause() {
        if (mGalleryView != null) {
            mGalleryView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGalleryView != null) {
            mGalleryView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mGalleryView != null) {
            mGalleryView.destroy();
        }
        super.onDestroy();
    }
}
