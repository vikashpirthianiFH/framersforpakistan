package com.innovpk.app.pkframes.ui;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.innovpk.app.pkframes.R;

import java.io.File;




public class SingleViewActivity extends AppCompatActivity {

    InterstitialAd mInterstitialAd;
    int mPositoin =0;
    DialogInterface.OnClickListener dialogClickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_view);
        mInterstitialAd = new InterstitialAd(this);
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            @Override
            public void onAdClosed() {

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdOpened() {

            }
        });

        ImageView displayIamge = (ImageView)findViewById(R.id.singleView);
        Bundle extras = getIntent().getExtras();

        mPositoin = extras.getInt("position");
        byte[] b = extras.getByteArray("picture");
        Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);

        displayIamge.setImageBitmap(getResizedBitmap(bmp,bmp.getHeight(),bmp.getWidth()));

        dialogClickListener   = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getString(R.string.folder_name));
                        File[] files = mediaStorageDir.listFiles();
                        if(files.length>0 && files != null) {
                            files[mPositoin].delete();
                            Intent galleryIntent = new Intent(SingleViewActivity.this, ImageGalleryActivity.class);
                            startActivity(galleryIntent);
                            finish();
                        }
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };



    }
    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
    public Bitmap getResizedBitmap(Bitmap bmp, int newHeight, int newWidth)
    {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Log.e("Value", ""+scaleHeight+" "+scaleWidth);
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, false);
        return resizedBitmap;


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Willst du das Bild wirklich löschen?").setPositiveButton("Ja", dialogClickListener)
                        .setNegativeButton("Nein", dialogClickListener).show();

                return  true;
            case R.id.share:

                final File mediaStorageDir2 = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getApplicationContext().getResources().getString(R.string.app_name));
                File[] shareFile = mediaStorageDir2.listFiles();

                Uri uriShare = Uri.parse("file://"+shareFile[mPositoin].getAbsolutePath());


                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, uriShare);
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "#uRnature");
                sharingIntent.setType("*/*");

                sharingIntent.addFlags(android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(sharingIntent, "Bild teilen"));

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent galleryIntent = new Intent(SingleViewActivity.this,ImageGalleryActivity.class);
        startActivity(galleryIntent);
    }
}